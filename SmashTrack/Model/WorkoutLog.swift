//
//  WorkoutLog.swift
//  SmashTrack
//
//  Created by Thenardo Ardo on 01/08/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import Foundation

class WorkoutLog {
    var dateTime: String
    var smashCount: Int
    var serveCount: Int
    var stepsCount: Int
    var caloriesBurnt: Int
    var commentWk: String
    
    init(dateTime: String, smashCount: Int, serveCount: Int, stepsCount: Int, caloriesBurnt: Int, commentWk: String) {
        self.dateTime = dateTime
        self.smashCount = smashCount
        self.serveCount = serveCount
        self.stepsCount = stepsCount
        self.caloriesBurnt = caloriesBurnt
        self.commentWk = commentWk
    }
}
