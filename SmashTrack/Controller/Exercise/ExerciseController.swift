//
//  ExerciseController.swift
//  SmashTrack
//
//  Created by Thenardo Ardo on 31/07/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class ExerciseController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var backGradView: UIView!
    @IBOutlet weak var exerciseCollection: UICollectionView!
    
    // MARK: - Variable
    
    var exerciseName = ["Beginner Serve", "Advanced Serve", "Intermediate Serve", "Beginner Lob", "Advanced Lob", "Intermediate Lob", "Basic Smash", "Expert Smash"]
    
    // MARK: - Initial & Cleaning Function
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setNavBarClear()
        setGradientBackground()
        exerciseCollection.delegate = self
        exerciseCollection.dataSource = self
        exerciseCollection.backgroundColor = UIColor.clear
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setGradientBackground() {
        let gradLayer = CAGradientLayer()
        gradLayer.frame = view.bounds
        gradLayer.colors = [UIColor(red: 27/255, green: 163/255, blue: 217/255, alpha: 1).cgColor, UIColor(red: 113/255, green: 0/255, blue: 233/255, alpha: 1).cgColor]
        gradLayer.shouldRasterize = true
        view.layer.insertSublayer(gradLayer, at: 0)
    }
    
    func setNavBarClear() {
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.view.backgroundColor = UIColor.clear
    }
    
    // MARK: - Features Logic Function
    
    func playTrainingVideo() {
        guard let url = URL(string: "https://devstreaming-cdn.apple.com/videos/streaming/examples/bipbop_adv_example_hevc/master.m3u8") else {
            return
        }
        
        let player = AVPlayer(url: url)
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    
    // MARK: - Collection Functions
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return exerciseName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "exerciseCell", for: indexPath) as! ExerciseCell
        
        let ex = exerciseName[indexPath.row]
        
        cell.exerciseName.text = ex
        
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let ex = exerciseName[indexPath.row]
        GlobalState.selectedExercise = ex
        playTrainingVideo()
    }
    
    // MARK: - Navigation

//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "exerciseModalSegue" {
//            let ex = segue.destination as! ExerciseModalController
//            ex.exName = sender as? String ?? ""
//        }
//    }
 

}
