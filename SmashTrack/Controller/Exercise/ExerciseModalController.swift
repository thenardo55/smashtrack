//
//  ExerciseModalController.swift
//  SmashTrack
//
//  Created by Thenardo Ardo on 02/08/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class ExerciseModalController: UIViewController {
    
    // WARNING! This is just a test page, will be deleted after final feature done
    
    @IBOutlet weak var exerciseName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        exerciseName.text = "\(GlobalState.selectedExercise)"
    }
    
    @IBAction func playAction(_ sender: UIButton) {
        guard let url = URL(string: "https://devstreaming-cdn.apple.com/videos/streaming/examples/bipbop_adv_example_hevc/master.m3u8") else {
            return
        }
        
        let player = AVPlayer(url: url)
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
        
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
