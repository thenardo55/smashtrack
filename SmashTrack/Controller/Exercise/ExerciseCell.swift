//
//  ExerciseCell.swift
//  SmashTrack
//
//  Created by Thenardo Ardo on 01/08/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class ExerciseCell: UICollectionViewCell {
    
    @IBOutlet weak var exerciseName: UILabel!
    
}
