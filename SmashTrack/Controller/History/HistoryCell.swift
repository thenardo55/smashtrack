//
//  HistoryCell.swift
//  SmashTrack
//
//  Created by Thenardo Ardo on 31/07/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell {

    @IBOutlet weak var backGradView: UIView!
    
    @IBOutlet weak var historyDate: UILabel!
    @IBOutlet weak var historySmashCount: UILabel!
    @IBOutlet weak var historyServeCount: UILabel!
    @IBOutlet weak var historyStepsCount: UILabel!
    @IBOutlet weak var historyCaloriesBurnt: UILabel!
    @IBOutlet weak var historyComment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
