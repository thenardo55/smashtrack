//
//  HistoryController.swift
//  SmashTrack
//
//  Created by Thenardo Ardo on 31/07/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class HistoryController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var backGradView: UIView!
    @IBOutlet weak var historyTable: UITableView!
    
    var workoutLog = [WorkoutLog]()
    
    // MARK: - Dummy data
    
    func loadDummyData() {
        let log1 = WorkoutLog(dateTime: "Today, 10:00 AM", smashCount: 740, serveCount: 1125, stepsCount: 1537, caloriesBurnt: 350, commentWk: "Amazing record! Brag this to your friend.")
        let log2 = WorkoutLog(dateTime: "Sunday, 12 June 2020, 10:00 AM", smashCount: 635, serveCount: 1099, stepsCount: 1324, caloriesBurnt: 312, commentWk: "Good job! Try to be better next time.")
        workoutLog += [log1, log2]
    }
    
    // MARK: - Initial Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBarClear()
        setGradientBackground()
        loadDummyData()
        historyTable.dataSource = self
        historyTable.delegate = self
        historyTable.backgroundColor = UIColor.clear
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setGradientBackground() {
        let gradLayer = CAGradientLayer()
        gradLayer.frame = view.bounds
        gradLayer.colors = [UIColor(red: 27/255, green: 163/255, blue: 217/255, alpha: 1).cgColor, UIColor(red: 113/255, green: 0/255, blue: 233/255, alpha: 1).cgColor]
        gradLayer.shouldRasterize = true
        view.layer.insertSublayer(gradLayer, at: 0)
    }
    
    func setNavBarClear() {
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.view.backgroundColor = UIColor.clear
    }
    
    // MARK: - Table Function
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workoutLog.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "historyCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
            as? HistoryCell else {
                fatalError("The dequened cell is not an instance of HistoryCell")
        }
        
        let wkLog = workoutLog[indexPath.row]
        
        cell.historyDate.text = wkLog.dateTime
        cell.historySmashCount.text = "\(wkLog.smashCount)"
        cell.historyServeCount.text = "\(wkLog.serveCount)"
        cell.historyStepsCount.text = "\(wkLog.stepsCount)"
        cell.historyCaloriesBurnt.text = "\(wkLog.caloriesBurnt)"
        cell.historyComment.text = wkLog.commentWk
        
        let cellGradLayer = CAGradientLayer()
        cellGradLayer.frame = cell.backGradView.bounds
        cellGradLayer.colors = [UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.21).cgColor, UIColor(red: 51/255, green: 88/255, blue: 151/255, alpha: 0.15).cgColor]
        cellGradLayer.shouldRasterize = true
        cell.backGradView.layer.addSublayer(cellGradLayer)
        
        cell.backgroundColor = UIColor.clear
        
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
