//
//  WorkoutController.swift
//  SmashTrack
//
//  Created by Thenardo Ardo on 31/07/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import UIKit

class WorkoutController: UIViewController {
    
    // MARK: - Outlet
    
    @IBOutlet weak var sessionTimerLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    
    // MARK: - Variable
    
    let circleLayer = CAShapeLayer()
    var sessionPaused = false
    var countdownTimer: Timer?
    var count = 3
    var runningSessionTimer: Timer?
    var workoutDuration = 0
    
    // MARK: - Initial & Cleaning Function
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavBarClear()
        setGradientBackground()
        generateStartCircle()
        pauseButton.tintColor = UIColor.white
        pauseButton.isHidden = true
        stopButton.tintColor = UIColor.white
        stopButton.isHidden = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func setGradientBackground() {
        let gradLayer = CAGradientLayer()
        gradLayer.frame = view.bounds
        gradLayer.colors = [UIColor(red: 27/255, green: 163/255, blue: 217/255, alpha: 1).cgColor, UIColor(red: 113/255, green: 0/255, blue: 233/255, alpha: 1).cgColor]
        gradLayer.shouldRasterize = true
        view.layer.insertSublayer(gradLayer, at: 0)
    
    }
    
    func setNavBarClear() {
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.view.backgroundColor = UIColor.clear
    }
    
    func generateStartCircle() {
        let strokeLayer = CAShapeLayer()
        //let centerPoint = view.center
        let circlePath = UIBezierPath(arcCenter: CGPoint.init(x: 205, y: 335), radius: 100, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true)
        
        // draw stroke layer
        strokeLayer.path = circlePath.cgPath
        strokeLayer.strokeColor = UIColor.lightGray.cgColor
        strokeLayer.lineWidth = 10
        strokeLayer.fillColor = UIColor.clear.cgColor
        strokeLayer.lineCap = CAShapeLayerLineCap.round
        view.layer.addSublayer(strokeLayer)
        
        // draw main circle layer
        circleLayer.path = circlePath.cgPath
        circleLayer.strokeColor = UIColor.white.cgColor
        circleLayer.lineWidth = 10
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.lineCap = CAShapeLayerLineCap.round
        circleLayer.strokeEnd = 0
        view.layer.addSublayer(circleLayer)
    }
    
    // MARK: - Feature Logic Functions
    
    func animateCircle() {
        let circleAnim = CABasicAnimation(keyPath: "strokeEnd")
        circleAnim.toValue = 1
        circleAnim.duration = 1
        circleAnim.fillMode = CAMediaTimingFillMode.forwards
        circleAnim.isRemovedOnCompletion = false
        circleLayer.add(circleAnim, forKey: "circleAnim")
    }
    
    @objc func startCountdown() {
        startButton.setTitle("\(count)", for: .normal)
        count -= 1
        animateCircle()
        
        if count < 0 {
            countdownTimer?.invalidate()
            countdownTimer = nil
            count = 3
            startButton.setTitle("GO!", for: .normal)
            startButton.isEnabled = false
            pauseButton.isHidden = false
            stopButton.isHidden = false
            startSessionTimer()
        }
    }
    
    func runSessionTimer() {
        runningSessionTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateSessionTimer), userInfo: nil, repeats: true)
    }
    
    func startSessionTimer() {
        runningSessionTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateSessionTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateSessionTimer() {
        workoutDuration += 1
        let ss = workoutDuration % 60
        let mn = (workoutDuration / 60) % 60
        let hh = workoutDuration / 3600
        sessionTimerLabel.text = String(format: "%02d:%02d:%02d", hh, mn, ss)
    }
    
    // MARK: - Action Functions

    @IBAction func startWorkout(_ sender: UIButton) {
        startButton.isEnabled = false
        countdownTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(startCountdown), userInfo: nil, repeats: true)
    }
    
    @IBAction func pauseSessionAction(_ sender: UIButton) {
        if sessionPaused {
            startButton.setTitle("GO!", for: .normal)
            sessionPaused = false
            runSessionTimer()
        } else {
            startButton.setTitle("PAUSED", for: .normal)
            sessionPaused = true
            runningSessionTimer?.invalidate()
        }
    }
    
    @IBAction func stopSessionAction(_ sender: UIButton) {
        startButton.setTitle("START", for: .normal)
        sessionPaused = false
        startButton.isEnabled = true
        pauseButton.isHidden = true
        stopButton.isHidden = true
        runningSessionTimer?.invalidate()
        workoutDuration = 0
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
