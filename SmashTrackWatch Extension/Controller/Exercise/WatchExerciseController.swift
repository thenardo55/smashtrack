//
//  WatchExerciseController.swift
//  SmashTrackWatch Extension
//
//  Created by Thenardo Ardo on 20/08/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import WatchKit
import Foundation


class WatchExerciseController: WKInterfaceController {

    @IBOutlet weak var exerciseButtonGroup: WKInterfaceGroup!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
