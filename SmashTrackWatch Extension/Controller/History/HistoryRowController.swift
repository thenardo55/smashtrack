//
//  HistoryRowController.swift
//  SmashTrackWatch Extension
//
//  Created by Thenardo Ardo on 19/08/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import WatchKit

class HistoryRowController: NSObject {
    @IBOutlet weak var workoutDate: WKInterfaceLabel!
    @IBOutlet weak var workoutStrokes: WKInterfaceLabel!
    @IBOutlet weak var workoutServes: WKInterfaceLabel!
    @IBOutlet weak var workoutSteps: WKInterfaceLabel!
    @IBOutlet weak var workoutCal: WKInterfaceLabel!
    
}
