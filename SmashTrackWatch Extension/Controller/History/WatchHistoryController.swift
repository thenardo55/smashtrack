//
//  WatchHistoryController.swift
//  SmashTrackWatch Extension
//
//  Created by Thenardo Ardo on 19/08/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import WatchKit
import Foundation

class WatchHistoryController: WKInterfaceController {
    
    @IBOutlet weak var sessionHistoryTable: WKInterfaceTable!
    
    var arrOfDummyHistory = [WorkoutHistory]()
    
    func loadDummy() {
        let history1 = WorkoutHistory(date: "Today, 15:15 PM", strokes: "23", serves: "12", steps: "221", cal: "543")
        let history2 = WorkoutHistory(date: "Sun, 12 Apr 2020 15:00 PM", strokes: "34", serves: "14", steps: "332", cal: "431")
        let history3 = WorkoutHistory(date: "Sat, 11 Apr 2020 13:32 PM", strokes: "43", serves: "23", steps: "133", cal: "566")
        arrOfDummyHistory += [history1, history2, history3]
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        loadDummy()
        sessionHistoryTable.setNumberOfRows(arrOfDummyHistory.count, withRowType: "sessionHistory")
        for index in 0..<arrOfDummyHistory.count {
            guard let controller = sessionHistoryTable.rowController(at: index) as? HistoryRowController else { continue }
            controller.workoutDate.setText(arrOfDummyHistory[index].date)
            controller.workoutStrokes.setText(arrOfDummyHistory[index].strokes)
            controller.workoutServes.setText(arrOfDummyHistory[index].serves)
            controller.workoutSteps.setText(arrOfDummyHistory[index].steps)
            controller.workoutCal.setText(arrOfDummyHistory[index].cal)
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
}
