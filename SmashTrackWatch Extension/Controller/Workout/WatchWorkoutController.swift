//
//  WatchWorkoutController.swift
//  SmashTrackWatch Extension
//
//  Created by Thenardo Ardo on 19/08/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import WatchKit
import Foundation


class WatchWorkoutController: WKInterfaceController {
    
    @IBOutlet weak var startButton: WKInterfaceButton!
    @IBOutlet weak var pauseButton: WKInterfaceButton!
    @IBOutlet weak var pauseBtnImg: WKInterfaceImage!
    @IBOutlet weak var stopButton: WKInterfaceButton!
    @IBOutlet weak var stopBtnImg: WKInterfaceImage!
    
    var countdownTimer: Timer?
    var count = 3
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        pauseBtnImg.setTintColor(UIColor.gray)
        stopBtnImg.setTintColor(UIColor.gray)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @objc func startCountdown() {
        startButton.setTitle("\(count)")
        count -= 1
        
        if count < 0 {
            countdownTimer?.invalidate()
            countdownTimer = nil
            count = 3
            startButton.setTitle("GO!")
            startButton.setEnabled(false)
            pauseButton.setEnabled(true)
            pauseBtnImg.setTintColor(UIColor.white)
            stopButton.setEnabled(true)
            stopBtnImg.setTintColor(UIColor.white)
            //startSessionTimer()
        }
    }
    
    @IBAction func startBtnAction() {
        startButton.setEnabled(false)
        countdownTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(startCountdown), userInfo: nil, repeats: true)
    }
    
    @IBAction func pauseBtnAction() {
        print("pressed pause")
    }
    
    @IBAction func stopBtnAction() {
        print("pressed stop")
        startButton.setEnabled(true)
        startButton.setTitle("START")
        pauseButton.setEnabled(false)
        pauseBtnImg.setTintColor(UIColor.gray)
        stopButton.setEnabled(false)
        stopBtnImg.setTintColor(UIColor.gray)
    }
    
}
