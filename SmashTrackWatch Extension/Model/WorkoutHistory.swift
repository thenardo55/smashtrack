//
//  WorkoutHistory.swift
//  SmashTrackWatch Extension
//
//  Created by Thenardo Ardo on 19/08/19.
//  Copyright © 2019 Thenardo Ardo. All rights reserved.
//

import Foundation

class WorkoutHistory {
    var date: String
    var strokes: String
    var serves: String
    var steps: String
    var cal: String
    
    init(date: String, strokes: String, serves: String, steps: String, cal: String) {
        self.date = date
        self.strokes = strokes
        self.serves = serves
        self.steps = steps
        self.cal = cal
    }
}
